package br.com.academico.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.academico.model.dados.Aluno;
import br.com.academico.model.dados.Curso;
import br.com.academico.model.dao.AlunoDAO;
import br.com.academico.model.dao.CursoDAO;

@RequestScoped
@ManagedBean
public class AlunoBean {
	private Aluno aluno;
	private Long idCursoSelecionado;
	private List<Aluno> alunos;
	private List<Curso> cursos;
	private AlunoDAO alunoDAO;
	private CursoDAO cursoDAO;
	private String mensagemRetorno;
	
	public AlunoBean() {
		this.aluno = new Aluno();
		this.alunoDAO = new AlunoDAO();
		this.cursoDAO = new CursoDAO();
		this.alunos = this.alunoDAO.lerTudo(Aluno.class);
		this.cursos = this.cursoDAO.lerTudo(Curso.class);
	}
	
	public Aluno getAluno() {
		return aluno;
	}
	
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	
	public Long getIdCursoSelecionado() {
		return idCursoSelecionado;
	}
	
	public void setIdCursoSelecionado(Long idCursoSelecionado) {
		this.idCursoSelecionado = idCursoSelecionado;
	}
	
	public String getMensagemRetorno() {
		return mensagemRetorno;
	}
	
	public void setMensagemRetorno(String mensagemRetorno) {
		this.mensagemRetorno = mensagemRetorno;
	}
	
	public List<Aluno> getAlunos() {
		return this.alunos;
	}
	
	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}
	
	public List<Curso> getCursos() {
		return cursos;
	}
	
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	
	public void salvar() {
		this.aluno.setCurso(new Curso(this.idCursoSelecionado, null));
		Aluno alunoRetorno = this.alunoDAO.salvar(aluno);
		if(alunoRetorno != null) {
			this.mensagemRetorno = "Aluno " + alunoRetorno.getNome() + " salvo com sucesso!";
		} else {
			this.mensagemRetorno =  "Erro ao salvar o aluno!";
		}
		
		this.aluno = new Aluno();
		this.idCursoSelecionado = 0L;
		this.alunos = this.alunoDAO.lerTudo(Aluno.class);
	}
	
	public void remover(Long id) {
		if(this.alunoDAO.remover(id, Aluno.class)) {
			this.mensagemRetorno = "Aluno de id " + id + " removido com sucesso!";
		} else {
			this.mensagemRetorno =  "Erro ao remover o aluno!";
		}
		
		this.alunos = this.alunoDAO.lerTudo(Aluno.class);
	}
	
	public String prepararEdicao(Aluno aluno, Long idCurso) {
		this.aluno = aluno;
		this.idCursoSelecionado = idCurso;

		return "edicao-aluno";
	}
}
