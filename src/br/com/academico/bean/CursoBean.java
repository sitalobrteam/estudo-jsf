package br.com.academico.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.academico.model.dados.Curso;
import br.com.academico.model.dao.CursoDAO;

@RequestScoped
@ManagedBean
public class CursoBean {
	private Curso curso;
	private List<Curso> cursos;
	private CursoDAO cursoDAO;
	private String mensagemRetorno;
	
	public CursoBean() {
		this.curso = new Curso();
		this.cursoDAO = new CursoDAO();
		this.cursos = this.cursoDAO.lerTudo(Curso.class);
	}
	
	public Curso getCurso() {
		return curso;
	}
	
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	
	public String getMensagemRetorno() {
		return mensagemRetorno;
	}
	
	public void setMensagemRetorno(String mensagemRetorno) {
		this.mensagemRetorno = mensagemRetorno;
	}
	
	public List<Curso> getCursos() {
		return cursos;
	}
	
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	
	public void salvar() {
		Curso cursoRetorno = this.cursoDAO.salvar(curso);
		if(cursoRetorno != null) {
			this.mensagemRetorno = "Curso " + cursoRetorno.getNome() + " salvo com sucesso!";
		} else {
			this.mensagemRetorno =  "Erro ao salvar o curso!";
		}
		
		this.curso = new Curso();
		this.cursos = this.cursoDAO.lerTudo(Curso.class);
	}
	
	public void remover(Long id) {
		if(this.cursoDAO.remover(id, Curso.class)) {
			this.mensagemRetorno = "Curso de id " + id + " removido com sucesso!";
		} else {
			this.mensagemRetorno =  "Erro ao remover o curso!";
		}
		
		this.cursos = this.cursoDAO.lerTudo(Curso.class);
	}
	
	public String prepararEdicao(Curso curso) {
		this.curso = curso;

		return "edicao-curso";
	}
}
