-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema escoladb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema escoladb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `escoladb` DEFAULT CHARACTER SET utf8 ;
USE `escoladb` ;

-- -----------------------------------------------------
-- Table `escoladb`.`tb_curso`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `escoladb`.`tb_curso` ;

CREATE TABLE IF NOT EXISTS `escoladb`.`tb_curso` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `escoladb`.`tb_aluno`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `escoladb`.`tb_aluno` ;

CREATE TABLE IF NOT EXISTS `escoladb`.`tb_aluno` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `matricula` VARCHAR(45) NOT NULL,
  `data_nascimento` TIMESTAMP NOT NULL,
  `fk_curso` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tb_aluno_tb_curso_idx` (`fk_curso` ASC),
  CONSTRAINT `fk_tb_aluno_tb_curso`
    FOREIGN KEY (`fk_curso`)
    REFERENCES `escoladb`.`tb_curso` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
