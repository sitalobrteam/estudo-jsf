package br.com.academico.model.dados;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Aluno
 *
 */

@Entity
@Table(name="tb_aluno")
public class Aluno implements Serializable, EntidadeBase {

	private static final long serialVersionUID = 1L;
	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String nome;
	@Column(nullable = false)
	private String matricula;
	@Column(nullable = false, name = "data_nascimento")
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;
	@ManyToOne
	@JoinColumn(name = "fk_curso")
	private Curso curso;

	public Aluno() {
		super();
	}
	
	public Aluno(Long id, String nome, String matricula, Date dataNascimento, Curso curso) {
		super();
		this.id = id;
		this.nome = nome;
		this.matricula = matricula;
		this.dataNascimento = dataNascimento;
		this.curso = curso;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getMatricula() {
		return this.matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public Date getDataNascimento() {
		return this.dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public Curso getCurso() {
		return curso;
	}
	
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
   
}
