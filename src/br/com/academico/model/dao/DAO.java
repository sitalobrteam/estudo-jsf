package br.com.academico.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;

import br.com.academico.model.dados.EntidadeBase;

public class DAO<T extends EntidadeBase> {
	
	public EntityManager getEntityManager() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("sistema-academico");
		return emf.createEntityManager();
	}
	
	public T salvar(T obj) {
		EntityManager em = getEntityManager();
		
		try {
			em.getTransaction().begin();
			
			if(obj.getId() == null) {
				em.persist(obj);
			} else {
				em.merge(obj);
			}
			
			em.getTransaction().commit();
		} catch(Exception ex) {
			em.getTransaction().rollback();
			ex.printStackTrace();
			
			return null;
		} finally {
			em.close();
		}
		
		return obj;
	}
	
	public Boolean remover(Long id, Class<T> c) {
		EntityManager em = getEntityManager();
		
		try {
			em.getTransaction().begin();
			
			T obj = em.find(c, id);
			em.remove(obj);
			
			em.getTransaction().commit();
		} catch (Exception ex) {
			em.getTransaction().rollback();
			ex.printStackTrace();
			
			return false;
		} finally {
			em.close();
		}
		
		return true;
	}
	
	public T lerPorId(Long id, Class<T> c) {
		EntityManager em = getEntityManager();
		T obj = null;
		
		try {
			obj = em.find(c, id);
		} catch (Exception ex) {
			em.getTransaction().rollback();
			ex.printStackTrace();
		} finally {
			em.close();
		}
		
		return obj;
	}
	
	public List<T> lerTudo(Class<T> c) {
		EntityManager em = getEntityManager();
		try {
			CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(c);
			query.select(query.from(c));
			
			return em.createQuery(query).getResultList();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
