package br.com.academico.teste;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import br.com.academico.model.dados.Aluno;
import br.com.academico.model.dados.Curso;
import br.com.academico.model.dao.AlunoDAO;
import br.com.academico.model.dao.CursoDAO;

public class TesteMain {

	public static void main(String[] args) {
		try {
			AlunoDAO aDAO = new AlunoDAO();
			CursoDAO cDAO = new CursoDAO();
			
			Curso curso = new Curso(null, "Direito");
			cDAO.salvar(curso);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
